﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PassHolder.Migrations
{
    public partial class AddGroupsToUserEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GroupsToUsers",
                columns: table => new
                {
                    GroupId = table.Column<int>(nullable: false),
                    AppUserId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    IsModerator = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupsToUsers", x => new { x.GroupId, x.AppUserId });
                    table.ForeignKey(
                        name: "FK_GroupsToUsers_AspNetUsers_AppUserId",
                        column: x => x.AppUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupsToUsers_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupsToUsers_AppUserId",
                table: "GroupsToUsers",
                column: "AppUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupsToUsers");
        }
    }
}
