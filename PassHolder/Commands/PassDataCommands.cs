﻿using MediatR;
using PassHolder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Commands
{
    public class AddPassDataCommand : IRequest<PassDataModel>
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
    }

    public class UpdatePassDataCommand : IRequest<PassDataModel>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
    }

    public class DeletePassDataCommand : INotification
    {
        public int Id { get; set; }
    }
}
