﻿using PassHolder.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PassHolder.Models;
using Microsoft.AspNetCore.Identity;
using PassHolder.Entities;
using System.Threading;
using AutoMapper;
using PassHolder.Queries;
using PassHolder.Repositories;
using Microsoft.EntityFrameworkCore;

namespace PassHolder.Handlers.Query
{
    public class AccountQueryHandlers : IRequestHandler<AllUsersQuery, List<UserModel>>
    {

        private readonly IMapper _mapper;
        private UserManager<AppUser> _userManager;
        private RoleManager<AppRole> _roleManager;

        public AccountQueryHandlers(IMapper mapper, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager)
        {
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task<List<UserModel>> Handle(AllUsersQuery request, CancellationToken cancellationToken)
        {

            var entities = await _userManager.Users.ToListAsync();

            var users = _mapper.Map<List<UserModel>>(entities);

            var roles = await _roleManager.Roles.ToListAsync();

            users.ForEach(u =>
            {
                u.role.Name = roles.First(r => r.Id == u.role.Id).Name;
            });

            return users;
        }
    }
}
