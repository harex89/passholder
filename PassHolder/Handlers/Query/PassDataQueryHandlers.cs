﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using PassHolder.Models;
using PassHolder.Queries;
using PassHolder.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolder.Handlers.Query
{
    public class PassDataQueryHandlers : 
        IRequestHandler<AllPassDataQuery, List<PassDataModel>>, 
        IRequestHandler<PassDataQuery, PassDataModel>
    {

        private readonly IPassDataRepository _passDataRepository;
        private readonly IMapper _mapper;

        public PassDataQueryHandlers(IPassDataRepository passDataRepository, IMapper mapper)
        {
            _passDataRepository = passDataRepository;
            _mapper = mapper;
        }

        public async Task<List<PassDataModel>> Handle(AllPassDataQuery request, CancellationToken cancellationToken)
        {

            var entities = await _passDataRepository.GetAllAsync();

            var passDatas = _mapper.Map<List<PassDataModel>>(entities);

            return passDatas;
        }

        public async Task<PassDataModel> Handle(PassDataQuery request, CancellationToken cancellationToken)
        {
            var entity = await _passDataRepository.GetAsync(request.Id);

            return _mapper.Map<PassDataModel>(entity);
        }
    }
}
