﻿using PassHolder.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PassHolder.Models;
using Microsoft.AspNetCore.Identity;
using PassHolder.Entities;
using System.Threading;
using AutoMapper;
using PassHolder.Queries;
using PassHolder.Repositories;

namespace PassHolder.Handlers.Query
{
    public class GroupQueryHandlers : IRequestHandler<AllGroupQuery, List<GroupModel>>
    {

        private readonly IMapper _mapper;
        private readonly IGroupsRepository _groupsRepository;

        public GroupQueryHandlers(IMapper mapper, IGroupsRepository groupsRepository)
        {
            _mapper = mapper;
            _groupsRepository = groupsRepository;
        }

        public async Task<List<GroupModel>> Handle(AllGroupQuery request, CancellationToken cancellationToken)
        {

            var entities = await _groupsRepository.GetAllAsync();
            var groups =  _mapper.Map<List<GroupModel>>(entities);

            foreach (var group in groups)
            {
                group.users = _mapper.Map<List<SimpleUserModel>>(entities.First(e => e.Id == group.Id).GroupsToUsers.ToList());
            }

            return groups;
        }
    }
}
