﻿using PassHolder.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PassHolder.Models;
using Microsoft.AspNetCore.Identity;
using PassHolder.Entities;
using System.Threading;
using AutoMapper;

namespace PassHolder.Handlers.Command
{
    public class AccountCommandHandlers : IRequestHandler<RegisterUserCommand, MassageModel>, IRequestHandler<LogInUserCommand, MassageModel>
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private readonly IMapper _mapper;

        public AccountCommandHandlers(UserManager<AppUser> userManager, SignInManager<AppUser> singInManager, IMapper mapper)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _mapper = mapper;
        }

        public async Task<MassageModel> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            MassageModel massage = new MassageModel();

            try
            {
                var appUser = _mapper.Map<AppUser>(request);

                if (request != null)
                {
                    IdentityResult result = await _userManager.CreateAsync(appUser, request.Password);
                    massage.succesed = result.Succeeded;
                    if (result.Succeeded)
                        massage.Message = "User was created";
                    else
                        result.Errors.ToList().ForEach(e =>
                            {
                                massage.Message = massage.Message + e.Description;
                            });
                }
            }
            catch (Exception ex)
            {
                massage.Message = ex.Message;
            }

            return massage;
        }

        public async Task<MassageModel> Handle(LogInUserCommand request, CancellationToken cancellationToken)
        {
            MassageModel massage = new MassageModel();
            var result = await _singInManager.PasswordSignInAsync(request.UserName, request.Password, false, false);

            massage.succesed = result.Succeeded;

            if (result.Succeeded)
            {
                massage.succesed = true;
            }
            else
            {
                massage.Message = "result is: " + result.ToString();
            }

            return massage;
        }
    }
}
