﻿using AutoMapper;
using MediatR;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;
using PassHolder.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PassHolder.Handlers.Command
{
    public class PassDataCommandHandlers : 
        IRequestHandler<AddPassDataCommand, PassDataModel>,
        IRequestHandler<UpdatePassDataCommand, PassDataModel>,
        INotificationHandler<DeletePassDataCommand>
    {
        private readonly IPassDataRepository _passDataRepository;
        private readonly IMapper _mapper;

        public PassDataCommandHandlers(IPassDataRepository passDataRepository, IMapper mapper)
        {
            _passDataRepository = passDataRepository;
            _mapper = mapper;
        }

        public async Task<PassDataModel> Handle(AddPassDataCommand request, CancellationToken cancellationToken)
        {
            var entity = await _passDataRepository.SaveAsync(_mapper.Map<PassData>(request));

            return _mapper.Map<PassDataModel>(entity);
        }

        public async Task<PassDataModel> Handle(UpdatePassDataCommand request, CancellationToken cancellationToken)
        {
            var entity = await _passDataRepository.SaveAsync(_mapper.Map<PassData>(request));

            return _mapper.Map<PassDataModel>(entity);
        }

        public async Task Handle(DeletePassDataCommand notification, CancellationToken cancellationToken)
        {
            await _passDataRepository.RemoveAsync(notification.Id);
        }
    }
}
