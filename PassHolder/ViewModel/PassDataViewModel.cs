﻿using PassHolder.Entities;
using PassHolder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.ViewModel
{
    public class PassDataViewModel
    {
        public List<PassDataModel> PassDatas { get; set; }

        public PassDataModel SelectedPassData { get; set; }
        public string DisplayMode { get; set; }
    }
}
