﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolder.Queries;

namespace PassHolder.Controllers
{
    public class AccountController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public AccountController(UserManager<AppUser> userManager, 
           SignInManager<AppUser> singInManager,
           RoleManager<AppRole> roleManager,
           IMapper mapper,
           IMediator mediator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
        }

        public async Task<IActionResult> Logout()
        {
            await _singInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Login(LogInUserModel command)
        {
            var massage = await _mediator.Send(_mapper.Map<LogInUserCommand>(command));

            if (massage.succesed)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Result = massage.Message;
            }

            return View();
        }

        public async Task<IActionResult> RegisterLoad()
        {
            RegisterUserModel registerUserModel = new RegisterUserModel();
            ViewBag.Title = "Registraton";
            registerUserModel.Roles = _roleManager.Roles.Where(r => r.Id != 1).Select(r => new RoleModel() {Id = r.Id, Name = r.Name } );

            return View("Registration", registerUserModel);
        }


        [HttpPost]
        public async Task<IActionResult> Register(RegisterUserModel command)
        {
            var massage = await _mediator.Send(_mapper.Map<RegisterUserCommand>(command));
            ViewBag.Message = massage.Message;
            return View();
        }


        
        public async Task<IActionResult> UsersLoad()
        {
            var massage = await _mediator.Send(new AllUsersQuery());
            
            return View("Users", massage);
        }
    }
}
