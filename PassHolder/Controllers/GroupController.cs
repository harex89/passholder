﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolder.Queries;

namespace PassHolder.Controllers
{
    public class GroupController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public GroupController(UserManager<AppUser> userManager, 
           SignInManager<AppUser> singInManager,
           RoleManager<AppRole> roleManager,
           IMapper mapper,
           IMediator mediator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
        }


        public async Task<IActionResult> GroupsLoad()
        {

            ViewBag.Title = "Groups";
            var groups = await _mediator.Send(new AllGroupQuery());


            return View("Groups", groups);
        }


        

    }
}
