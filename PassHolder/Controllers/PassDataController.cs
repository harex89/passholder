﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PassHolder.Queries;
using PassHolder.ViewModel;
using Microsoft.AspNetCore.Antiforgery;

namespace PassHolder.Controllers
{
    public class PassDataController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _singInManager;
        private RoleManager<AppRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public PassDataController(UserManager<AppUser> userManager, 
           SignInManager<AppUser> singInManager,
           RoleManager<AppRole> roleManager,
           IMapper mapper,
           IMediator mediator)
        {
            _userManager = userManager;
            _singInManager = singInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _mediator = mediator;
        }


        public async Task<IActionResult> PassDataLoad()
        {

            ViewBag.Title = "PassData";
            var passData = await _mediator.Send(new AllPassDataQuery());

            var passDataVM = new PassDataViewModel() { PassDatas = passData };
            passDataVM.SelectedPassData = null;

            return View("PassData", passDataVM);
        }

        [HttpPost]
        public async Task<IActionResult> New()
        {

            ViewBag.Title = "PassData";
            var passData = await _mediator.Send(new AllPassDataQuery());

            var passDataVM = new PassDataViewModel() { PassDatas = passData };
            passDataVM.SelectedPassData = null;
            passDataVM.DisplayMode = "WriteOnly";

            return View("PassData", passDataVM);
        }

        [HttpPost]
        public async Task<IActionResult> Insert(PassDataModel newPassData)
        {

            var passData = await _mediator.Send(new AddPassDataCommand() 
            { 
                Login = newPassData.Login,
                Name = newPassData.Name,
                Password = newPassData.Password,
                Description = newPassData.Description
            });

            var passDatas = await _mediator.Send(new AllPassDataQuery());

            var model = new PassDataViewModel() { PassDatas = passDatas };

            model.SelectedPassData = passData;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Select(string id)
        {
            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery());
            model.SelectedPassData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(id) });
            model.DisplayMode = "ReadOnly";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(string id)
        {
            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery());
            model.SelectedPassData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(id) });
            model.DisplayMode = "ReadWrite";

            return View("PassData", model);
            
        }

        [HttpPost]
        public async Task<IActionResult> Update(PassDataModel obj)
        {

            var passData = await _mediator.Send(new UpdatePassDataCommand()
            {
                Id = obj.Id,
                Login = obj.Login,
                Name = obj.Name,
                Password = obj.Password,
                Description = obj.Description
            });

            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery());

            model.SelectedPassData = passData;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {

            await _mediator.Publish(new DeletePassDataCommand() { Id = int.Parse(id) });
            
            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery());

            model.SelectedPassData = null;
            model.DisplayMode = "";

            return View("PassData", model);
        }

        [HttpPost]
        public async Task<IActionResult> Cancel(string id)
        {
            _mediator.Publish(new DeletePassDataCommand() { Id = int.Parse(id) });

            PassDataViewModel model = new PassDataViewModel();

            model.PassDatas = await _mediator.Send(new AllPassDataQuery());

            model.SelectedPassData = model.SelectedPassData = await _mediator.Send(new PassDataQuery() { Id = int.Parse(id) });
            model.DisplayMode = "";

            return View("PassData", model);
        }

    }
}
