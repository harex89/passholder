﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Routing.Constraints;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;


namespace PassHolder.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            AllowNullCollections  = true;

            CreateMap<GroupModel, Group>();

            CreateMap<Group, GroupModel>();



            CreateMap<SimpleUserModel, GroupToUser>()
                .ForMember(dest => dest.IsModerator, opt => opt.MapFrom(src => src.IsAdmin))
                .ForMember(dest => dest.AppUserId, opt => opt.MapFrom(src => src.Id))
                .ForPath(dest => dest.AppUser.UserName, opt => opt.MapFrom(src => src.UserName))                
                .ReverseMap();
        }
    }
}
