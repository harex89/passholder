﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Routing.Constraints;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;


namespace PassHolder.Profiles
{
    public class PassDataProfile : Profile
    {
        public PassDataProfile()
        {
            AllowNullCollections  = true;

            CreateMap<PassDataModel, PassData>()
                .ReverseMap();

            CreateMap<AddPassDataCommand, PassData>()
                .ReverseMap();

            CreateMap<UpdatePassDataCommand, PassData>()
                .ReverseMap();
        }
    }
}
