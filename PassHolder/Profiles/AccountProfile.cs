﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using PassHolder.Commands;
using PassHolder.Entities;
using PassHolder.Models;


namespace PassHolder.Profiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<AppUser, RegisterUserCommand>()
                .ReverseMap();
            CreateMap<RegisterUserModel, RegisterUserCommand>()
                .ReverseMap();
            CreateMap<LogInUserCommand, LogInUserModel>()
                .ReverseMap();
            CreateMap<UserModel, AppUser>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                    .ForPath(dest => dest.RoleId, opt => opt.MapFrom(src => src.role.Id))
                .ReverseMap();
        }
    }
}
