﻿using PassHolder.Entities;
using PassHolder.Exceptions;
using PassHolder.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Group = PassHolder.Entities.Group;

namespace PassHolder.Repositories
{
    public interface IGroupsRepository
    {
        Task<List<Group>> GetAllAsync();
        Task<Group> SaveAsync(Group group);
        Task RemoveAsync(int groupId);
    }
    public class GroupsRepository : IGroupsRepository
    {
        private readonly ApplicationDbContext _context;

        public GroupsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Group>> GetAllAsync()
        {  
            return await _context.Groups
                .Include(g => g.GroupsToUsers)
                .ThenInclude(gtu => gtu.AppUser)
                .Include(g => g.GroupsToPasses)
                .ThenInclude(gtp => gtp.PassData)
                .ToListAsync();
        }

        public Task RemoveAsync(int groupId)
        {
            throw new NotImplementedException();
        }

        public Task<Group> SaveAsync(Group group)
        {
            throw new NotImplementedException();
        }
    }
}
