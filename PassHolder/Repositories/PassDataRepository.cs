﻿using PassHolder.Entities;
using PassHolder.Exceptions;
using PassHolder.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace PassHolder.Repositories
{
    public interface IPassDataRepository
    {
        Task<List<PassData>> GetAllAsync();
        Task<PassData> GetAsync(int id);
        Task<PassData> SaveAsync(PassData passData);
        Task RemoveAsync(int id);
    }
    public class PassDataRepository : IPassDataRepository
    {
        private readonly ApplicationDbContext _context;

        public PassDataRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<PassData>> GetAllAsync()
        {
            return await _context.PassDatas.AsNoTracking().ToListAsync();
        }

        public async Task<PassData> GetAsync(int id)
        {
            return await _context.PassDatas.AsNoTracking().FirstOrDefaultAsync(p => p.Id == id);
        }

        public  Task RemoveAsync(int id)
        {
            var passDataToDelete =  _context.PassDatas.AsNoTracking().FirstOrDefault(r => r.Id == id);
            if (passDataToDelete == null)
            {
                return  Task.CompletedTask;
            }
            _context.PassDatas.Remove(passDataToDelete);
            return _context.SaveChangesAsync();

        }

        public async Task<PassData> SaveAsync(PassData passData)
        {
            if (passData.Id == 0)
            {
                _context.Add(passData);
            }
            else
            {
                _context.Update(passData);
            }

            await _context.SaveChangesAsync();

            return passData;
        }
    }
}
