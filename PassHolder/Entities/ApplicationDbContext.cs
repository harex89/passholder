﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Entities
{
    public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, int>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public ApplicationDbContext()
        { 
        }

        public DbSet<PassData> PassDatas { get; set; }
        public DbSet<GroupsToPass> GroupsToPasses { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupToUser> GroupsToUsers { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GroupsToPass>()
                .HasKey(gtp => new { gtp.GroupId, gtp.PassDataId });
            modelBuilder.Entity<GroupsToPass>()
                .HasOne(gtp => gtp.Group)
                .WithMany(g => g.GroupsToPasses)
                .HasForeignKey(gtp => gtp.GroupId);
            modelBuilder.Entity<GroupsToPass>()
                .HasOne(gtp => gtp.PassData)
                .WithMany(pd => pd.GroupsToPasses)
                .HasForeignKey(gtp => gtp.PassDataId);

            modelBuilder.Entity<GroupToUser>()
               .HasKey(gtu => new { gtu.GroupId, gtu.AppUserId });
            modelBuilder.Entity<GroupToUser>()
                .HasOne(gtu => gtu.Group)
                .WithMany(g => g.GroupsToUsers)
                .HasForeignKey(gtu => gtu.GroupId);
            modelBuilder.Entity<GroupToUser>()
                .HasOne(gtu => gtu.AppUser)
                .WithMany(au => au.GroupsToUsers)
                .HasForeignKey(gtu => gtu.AppUserId);

        }

        }
    }
