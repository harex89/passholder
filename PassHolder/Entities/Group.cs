﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Entities
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual IEnumerable<GroupsToPass> GroupsToPasses { get; set; }
        public virtual IEnumerable<GroupToUser> GroupsToUsers { get; set; }
    }
}
