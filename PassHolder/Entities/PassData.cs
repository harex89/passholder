﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Entities
{
    public class PassData
    {
        public int Id { get; set; }
        public int AppUserId { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual IEnumerable<GroupsToPass> GroupsToPasses { get; set; }
    }
}
