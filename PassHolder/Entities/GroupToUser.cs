﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Entities
{
    public class GroupToUser
    {
        public int Id { get; set; }
        public bool IsModerator { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }
    }
}
