﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Entities
{
    public class GroupsToPass
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public Group Group { get; set; }
        public int PassDataId { get; set; }
        public PassData PassData { get; set; }
    }
}
