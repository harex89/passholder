﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Models
{
    public class GroupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<SimpleUserModel> users { get; set; }
    }
}
