﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Models
{
    public class LogInUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
