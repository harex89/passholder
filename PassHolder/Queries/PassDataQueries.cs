﻿using MediatR;
using PassHolder.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PassHolder.Queries
{
    public class AllPassDataQuery : IRequest<List<PassDataModel>>
    {
    }

    public class PassDataQuery : IRequest<PassDataModel>
    {
        public int Id { get; set; }
    }
}
