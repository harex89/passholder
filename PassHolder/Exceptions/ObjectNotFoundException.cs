﻿using System;

namespace PassHolder.Exceptions
{
    public abstract class ObjectNotFoundException : Exception
    {
        public ObjectNotFoundException(string message) : base(message)
        {
        }
    }

    public class ConfigrationTemplateNotFoundException : ObjectNotFoundException
    {
        public ConfigrationTemplateNotFoundException(int userId) : base($"Configuration with given UserID: {userId} does not exist")
        {
            UserId = userId;
        }

        public int UserId { get; set; }
    }

}